package com.devsummit.android.pointforplastic.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rram2303 on 11/24/16.
 */

public class Shop {

    @SerializedName("id")
    private int mId;

    @SerializedName("points")
    private int mPoints;

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public int getPoints() {
        return mPoints;
    }

    public void setPoints(int mPoints) {
        this.mPoints = mPoints;
    }
}
