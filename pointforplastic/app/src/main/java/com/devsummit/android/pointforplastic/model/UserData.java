package com.devsummit.android.pointforplastic.model;

/**
 * Created by rram2303 on 11/24/16.
 */

public class UserData {

    private String mFirstname;
    private String mSurname;
    private String mAddress;
    private String mPhone;

    public String getmFirstname() {
        return mFirstname;
    }

    public void setFirstname(String mFirstname) {
        this.mFirstname = mFirstname;
    }

    public String getSurname() {
        return mSurname;
    }

    public void setSurname(String mSurname) {
        this.mSurname = mSurname;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String mPhone) {
        this.mPhone = mPhone;
    }
}
