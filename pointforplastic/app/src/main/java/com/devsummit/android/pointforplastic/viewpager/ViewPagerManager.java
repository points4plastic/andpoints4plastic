package com.devsummit.android.pointforplastic.viewpager;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import com.devsummit.android.pointforplastic.R;
import com.devsummit.android.pointforplastic.qrscanner.QRFragment_;
import com.devsummit.android.pointforplastic.score.ScoreFragment_;
import com.devsummit.android.pointforplastic.statistics.StatisticFragment_;
import com.devsummit.android.pointforplastic.user.UserFragment_;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

/**
 * Created by vkos2006 on 11/24/16.
 */
@EBean
public class ViewPagerManager {

    @RootContext
    protected Context mContext;

    private ViewPagerAdapter mViewPagerAdapter;

    public void init(ViewPager viewPager,ViewPagerAdapter viewPagerAdapter){
        mViewPagerAdapter = viewPagerAdapter;
        mViewPagerAdapter.addFragment(ScoreFragment_.builder().build(),mContext.getString(R.string.lbl_tab_score));
        mViewPagerAdapter.addFragment(StatisticFragment_.builder().build(),mContext.getString(R.string.lbl_tab_statistics));
        mViewPagerAdapter.addFragment(QRFragment_.builder().build(),mContext.getString(R.string.lbl_tab_qr));
        mViewPagerAdapter.addFragment(UserFragment_.builder().build(),mContext.getString(R.string.lbl_tab_user));
        viewPager.setAdapter(mViewPagerAdapter);
    }

    public ViewPagerAdapter getViewPagerAdapter() {
        return mViewPagerAdapter;
    }

    public void setViewPagerAdapter(ViewPagerAdapter mViewPagerAdapter) {
        this.mViewPagerAdapter = mViewPagerAdapter;
    }

    public void addFragment(Fragment fragment, String title){

        mViewPagerAdapter.addFragment(fragment,title);
    }

}
