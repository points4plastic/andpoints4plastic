package com.devsummit.android.pointforplastic.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rram2303 on 11/24/16.
 */

public class Type {

    private static final int PFANG = 1;
    private static final int PLASTIK = 2;
    private static final int OHNE = 3;


    @SerializedName("type")
    private int mType;

    @SerializedName("value")
    private int mValue;

    public int getType() {
        return mType;
    }

    public void setType(int mType) {
        this.mType = mType;
    }

    public int getValue() {
        return mValue;
    }

    public void setValue(int mValue) {
        this.mValue = mValue;
    }
}
