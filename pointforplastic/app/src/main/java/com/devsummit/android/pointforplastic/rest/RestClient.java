package com.devsummit.android.pointforplastic.rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rram2303 on 11/24/16.
 */

public class RestClient {

    private static final String BASE_URL = "http://p4p.apps.ose.sademo.de";
    public static final String API_KEY = "7604b11b6a4c54554a2abb1057d84992";
    private static Retrofit retrofit;

    public static Retrofit getClient(){
        if(retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit;
    }
}
