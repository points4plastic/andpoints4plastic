package com.devsummit.android.pointforplastic.login;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.devsummit.android.pointforplastic.MainActivity_;
import com.devsummit.android.pointforplastic.R;
import com.devsummit.android.pointforplastic.register.RegistrationActivity_;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_login)
public class LoginActivity extends AppCompatActivity {

    public static final int REGISTER_REQUEST_CODE=100;


    @ViewById(R.id.login_username)
    protected EditText mUsername;

    @ViewById(R.id.login_password)
    protected EditText mPassword;

    @ViewById(R.id.login_btn)
    protected Button mLoginBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    private void launchMain() {
        Intent intent = new Intent(this, MainActivity_.class);
        startActivity(intent);
        finish();
    }

    private void launchRegister(){
        Intent intent = new Intent(this, RegistrationActivity_.class);
        startActivityForResult(intent,REGISTER_REQUEST_CODE);
    }


    @Click(R.id.login_btn)
    protected void loginClicked() {
        hideKeyboard();

        int usernameLength = mUsername.getText().length();
        int passwordLength = mPassword.getText().length();

        if (usernameLength > 3 && passwordLength > 6) {
            launchMain();
        } else {
            showLoginErrorDialog();
        }
    }

    @Click(R.id.register_btn)
    protected void registerClicked(){

        launchRegister();
    }

    private void hideKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void showLoginErrorDialog() {
        new AlertDialog.Builder(this)
                .setTitle("Login failed")
                .setMessage("User credentials doesn not match")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==REGISTER_REQUEST_CODE){


        }
    }
}
