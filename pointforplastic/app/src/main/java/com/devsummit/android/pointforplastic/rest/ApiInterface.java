package com.devsummit.android.pointforplastic.rest;

import com.devsummit.android.pointforplastic.model.Stats;
import com.devsummit.android.pointforplastic.model.UserData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by rram2303 on 11/24/16.
 */

public interface ApiInterface {

    @GET("stats")
    Call<Stats> getStats(@Query("user_key") String apiKey);
    
    Call<UserData> getUserData(@Query("user_key") String apiKey);

}
