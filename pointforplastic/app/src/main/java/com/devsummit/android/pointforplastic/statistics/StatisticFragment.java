package com.devsummit.android.pointforplastic.statistics;

import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;

import com.devsummit.android.pointforplastic.PFPFragment;
import com.devsummit.android.pointforplastic.R;
import com.devsummit.android.pointforplastic.model.Stats;
import com.devsummit.android.pointforplastic.rest.ApiInterface;
import com.devsummit.android.pointforplastic.rest.RestClient;
import com.devsummit.android.pointforplastic.statistics.adapter.StatisticAdapter;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.devsummit.android.pointforplastic.rest.RestClient.API_KEY;
import java.util.ArrayList;


/**
 * Created by anen2006 on 11/24/16.
 */
@EFragment(R.layout.fragment_statistics)
public class StatisticFragment extends Fragment implements PFPFragment {

    private static final String TAG = StatisticFragment.class.getSimpleName();

    private Stats mStats;

	@ViewById(R.id.statistic_list)
	protected RecyclerView mStatisticList;

	@ViewById(R.id.pieChart1)
	protected PieChart pieChart;

	@AfterViews
	public void init () {

		pieChart.getDescription().setEnabled(false);

		Typeface font = Typeface.DEFAULT_BOLD;

		pieChart.setCenterTextTypeface(font);
		pieChart.setCenterText(generateCenterText());
		pieChart.setCenterTextSize(10f);
		pieChart.setCenterTextTypeface(font);

		// radius of the center hole in percent of maximum radius
		pieChart.setHoleRadius(45f);
		pieChart.setTransparentCircleRadius(50f);

		Legend l = pieChart.getLegend();
		l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
		l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
		l.setOrientation(Legend.LegendOrientation.VERTICAL);
		l.setDrawInside(false);

		pieChart.getLegend().setEnabled(false);

		pieChart.setData(generatePieData());
		loadData();
	}

	protected PieData generatePieData() {

		int count = 3;

		ArrayList<PieEntry> entries1 = new ArrayList<>();

        entries1.add(new PieEntry((float) ((Math.random() * 50) + 10), "OHNE"));
        entries1.add(new PieEntry((float) ((Math.random() * 50) + 10), "PLASTIK"));
        entries1.add(new PieEntry((float) ((Math.random() * 50) + 10), "PFANG"));

        /*
		for(int i = 0; i < count; i++) {
			entries1.add(new PieEntry((float) ((Math.random() * 50) + 30), ""));
		}
		*/

		PieDataSet ds1 = new PieDataSet(entries1, "HAUTE 54 g");
		ds1.setColors(ColorTemplate.PASTEL_COLORS);
		ds1.setValueTextColor(Color.WHITE);
		ds1.setValueTextSize(12f);

		PieData d = new PieData(ds1);
		return d;
	}

	@Bean
	protected StatisticAdapter mStatisticAdapter;

	@Override
	public Fragment getFragment() {
		return this;
	}

	@Override
	public String getFragmentTag() {
		return this.getClass().getName();
	}

    @AfterViews
    protected void afterViews(){
        loadData();
    }

	@Override
	public void loadData() {

        getData();

		if (mStatisticAdapter != null){
			mStatisticAdapter = null;
		}
		RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
		mStatisticList.setLayoutManager(mLayoutManager);
		mStatisticList.setItemAnimator(new DefaultItemAnimator());
		mStatisticList.setAdapter(mStatisticAdapter);

	}

	@Override
	public void generateMockData() {

	}

	private void getData() {
		ApiInterface apiService = RestClient.getClient().create(ApiInterface.class);
		Call<Stats> call = apiService.getStats(API_KEY);
		call.enqueue(new Callback<Stats>() {
			@Override
			public void onResponse(Call<Stats> call, Response<Stats> response) {
                mStats = response.body();
                Log.d(TAG, "CO2Total: " + mStats.getCo2Total());
            }

			@Override
			public void onFailure(Call<Stats> call, Throwable t) {
                Log.d(TAG, t.getMessage());
			}
		});
	}

	private SpannableString generateCenterText() {
		SpannableString s = new SpannableString("HAUTE\n54 g");
		s.setSpan(new RelativeSizeSpan(2f), 0, 8, 0);
		s.setSpan(new ForegroundColorSpan(Color.BLACK), 8, s.length(), 0);
		return s;

	}
}
