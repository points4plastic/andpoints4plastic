package com.devsummit.android.pointforplastic.user;

import android.support.v4.app.Fragment;
import android.widget.EditText;

import com.devsummit.android.pointforplastic.PFPFragment;
import com.devsummit.android.pointforplastic.R;
import com.devsummit.android.pointforplastic.model.UserData;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by vkos2006 on 11/24/16.
 */
@EFragment(R.layout.fragment_user)
public class UserFragment extends Fragment implements PFPFragment{

    private UserData mUser;

    @ViewById(R.id.user_name)
    protected EditText mName;

    @ViewById(R.id.user_surname)
    protected EditText mSurname;

    @ViewById(R.id.user_address)
    protected EditText mAddress;

    @ViewById(R.id.user_phonenumber)
    protected EditText mPhone;

    @AfterViews
    protected void afterViews(){
        generateMockData();
    }


    @Override
    public Fragment getFragment() {
        return this;
    }

    @Override
    public String getFragmentTag() {
        return this.getClass().getName();
    }

    @Override
    public void loadData() {
        fillinViews();
    }

    @Override
    public void generateMockData() {
        mUser = new UserData();
        mUser.setFirstname("John");
        mUser.setSurname("Doe");
        mUser.setAddress("123 street, Seaview");
        mUser.setPhone("1234566789");

        fillinViews();
    }

    private void fillinViews(){
        mName.setText(mUser.getmFirstname());
        mSurname.setText(mUser.getSurname());
        mAddress.setText(mUser.getAddress());
        mPhone.setText(mUser.getPhone());
    }

}
