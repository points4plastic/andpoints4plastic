package com.devsummit.android.pointforplastic;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.devsummit.android.pointforplastic.viewpager.ViewPagerAdapter;
import com.devsummit.android.pointforplastic.viewpager.ViewPagerManager;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {



    @ViewById(R.id.toolbar)
    protected Toolbar mToolbar;

    @ViewById(R.id.tabs)
    protected TabLayout mTabLayout;

    @ViewById(R.id.viewpager)
    protected ViewPager mViewPager;

    @Bean
    protected ViewPagerManager mViewPagerManager;


    @AfterViews
    protected  void init(){
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationIcon(null);
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPagerManager.init(mViewPager,new ViewPagerAdapter(getSupportFragmentManager()));
    }

}
