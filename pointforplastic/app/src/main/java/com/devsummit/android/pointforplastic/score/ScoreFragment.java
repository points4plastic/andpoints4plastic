package com.devsummit.android.pointforplastic.score;

import android.support.v4.app.Fragment;

import com.devsummit.android.pointforplastic.PFPFragment;
import com.devsummit.android.pointforplastic.R;

import org.androidannotations.annotations.EFragment;

/**
 * Created by vkos2006 on 11/24/16.
 */

@EFragment(R.layout.fragment_score)
public class ScoreFragment extends Fragment implements PFPFragment{
    @Override
    public Fragment getFragment() {
        return this;
    }

    @Override
    public String getFragmentTag() {
        return this.getClass().getName();
    }

    @Override
    public void loadData() {

    }

    @Override
    public void generateMockData() {

    }
}
