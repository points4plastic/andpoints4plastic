package com.devsummit.android.pointforplastic.register;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.devsummit.android.pointforplastic.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_registration)
public class RegistrationActivity extends AppCompatActivity {

    @ViewById(R.id.toolbar)
    protected Toolbar mToolbar;

    @ViewById(R.id.first_name)
    protected TextView mFirstName;

    @ViewById(R.id.last_name)
    protected TextView mLastName;

    @ViewById(R.id.user_address)
    protected TextView mAddress;

    @ViewById(R.id.phone)
    protected TextView mPhone;




    @AfterViews
    protected void afterViews(){
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            setResult(RESULT_CANCELED);
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Click(R.id.register_btn)
    protected void register(){

        setResult(RESULT_OK);
        finish();
    }

}
