package com.devsummit.android.pointforplastic.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rram2303 on 11/24/16.
 */

public class Stats {

    @SerializedName("co2total")
    private int mCo2Total;

    @SerializedName("type")
    private List<Type> mTypeListl;

    @SerializedName("shops")
    private List<Shop> mShopList;

    public int getCo2Total() {
        return mCo2Total;
    }

    public void setCo2Total(int mCo2Total) {
        this.mCo2Total = mCo2Total;
    }

    public List<Type> getTypeListl() {
        return mTypeListl;
    }

    public void setTypeListl(List<Type> mTypeListl) {
        this.mTypeListl = mTypeListl;
    }

    public List<Shop> getShopList() {
        return mShopList;
    }

    public void setShopList(List<Shop> mShopList) {
        this.mShopList = mShopList;
    }
}
