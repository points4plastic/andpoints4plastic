package com.devsummit.android.pointforplastic.statistics.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.devsummit.android.pointforplastic.R;


import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;


/**
 * Created by anen2006 on 11/24/16.
 */

@EBean
public class StatisticAdapter extends RecyclerView.Adapter<StatisticAdapter.StatisticView> {


	@RootContext
	Context mContext;


	@Override
	public StatisticView onCreateViewHolder(ViewGroup parent, int viewType) {

		View itemView = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.statistic_view_layout, parent, false);
		return new StatisticView(itemView);
	}

	@Override
	public void onBindViewHolder(StatisticView holder, int position) {

		holder.mStatTextView.setText("");
		Glide
				.with(mContext)
				.load("image_url")
				.into(holder.mStatItemImage);

	}

	@Override
	public int getItemCount() {
		return 0;
	}

	public class StatisticView extends RecyclerView.ViewHolder {

		protected ImageView mStatItemImage;
		protected TextView mStatTextView;

		public StatisticView(View itemView) {
			super(itemView);

			mStatItemImage = (ImageView)itemView.findViewById(R.id.product_image);
			mStatTextView = (TextView)itemView.findViewById(R.id.statistic_text);
		}
	}

}
