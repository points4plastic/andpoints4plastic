package com.devsummit.android.pointforplastic.qrscanner;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.TextView;

import com.devsummit.android.pointforplastic.PFPFragment;
import com.devsummit.android.pointforplastic.R;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import static android.app.Activity.RESULT_OK;

/**
 * Created by vkos2006 on 11/24/16.
 */
@EFragment(R.layout.fragment_qr)
public class QRFragment extends Fragment implements PFPFragment {

    private static final int SCAN_REQUEST_CODE=0;

    @ViewById(R.id.scanning_result)
    protected TextView mScanningResult;

    @Override
    public Fragment getFragment() {
        return this;
    }

    @Override
    public String getFragmentTag() {
        return this.getClass().getName();
    }

    @Override
    public void loadData() {



    }

    @Override
    public void generateMockData() {

    }

    @Click(R.id.btn_scan_qr_code)
    public void click(){
        scanQrCode();
    }

    private void scanQrCode(){

        if(mScanningResult.getVisibility()==View.VISIBLE){
            mScanningResult.setVisibility(View.GONE);
        }
        try {

            Intent intent = new Intent("com.google.zxing.client.android.SCAN");
            intent.putExtra("SCAN_MODE", "QR_CODE_MODE");

            startActivityForResult(intent, SCAN_REQUEST_CODE);

        } catch (Exception e) {

            Uri marketUri = Uri.parse("market://details?id=com.google.zxing.client.android");
            Intent marketIntent = new Intent(Intent.ACTION_VIEW,marketUri);
            startActivity(marketIntent);

        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==SCAN_REQUEST_CODE){
            mScanningResult.setVisibility(View.VISIBLE);

            if(resultCode==RESULT_OK){

                String result =data.getStringExtra("SCAN_RESULT");
                mScanningResult.setText(result);
            }else{
                mScanningResult.setText("Scanning canceled.");
            }
        }
    }
}
