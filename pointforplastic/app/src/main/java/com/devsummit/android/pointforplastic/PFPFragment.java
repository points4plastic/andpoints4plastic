package com.devsummit.android.pointforplastic;

import android.support.v4.app.Fragment;

/**
 * Created by anen2006 on 11/24/16.
 */
public interface PFPFragment {

	Fragment getFragment();

	boolean isVisible();

	String getFragmentTag();

	boolean isAdded();

	void loadData();

	void generateMockData();
}
